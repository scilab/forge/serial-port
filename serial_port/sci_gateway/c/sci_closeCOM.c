//=============================================================================
// Allan CORNET
// DIGITEO - 2010
// CeCill
//=============================================================================
#include "ports_manager.h"
//=============================================================================
#include "api_scilab.h"
#include "localization.h"
#include "Scierror.h"
//=============================================================================
int sci_closeCOM(char *fname)
{
	SciErr sciErr;
	int *piAddressVar = NULL;
	double dVar = 0;
	int result = (int)FALSE;

	CheckRhs(1,1) ;
	CheckLhs(1,1) ;

	initializeStructPorts();

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVar);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if(getScalarDouble(pvApiCtx, piAddressVar, &dVar))
	{
		printError(&sciErr, 0);
		Scierror(999,"%s: Wrong size for input argument #%d: A scalar expected.\n", fname, 1);
		return 0;
	}

	result = closePortCOM((int)dVar);

	if(createScalarBoolean(pvApiCtx, Rhs + 1, &result))
	{
		printError(&sciErr, 0);
		return 0;
	}

	LhsVar(1) = Rhs + 1;
    PutLhsVar();

	return 0;
}
//=============================================================================
