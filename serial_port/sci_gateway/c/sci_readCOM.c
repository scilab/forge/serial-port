//=============================================================================
// Allan CORNET
// DIGITEO - 2010
// CeCill
//=============================================================================
#include "ports_manager.h"
//=============================================================================
#include "api_scilab.h"
#include "localization.h"
#include "Scierror.h"
#include "sciprint.h"
//=============================================================================
#define PARAM3_STRING "string"
#define PARAM3_BYTES "bytes"
//=============================================================================
static double* bytesToDoubles(BYTE *pBytes, int iSize);
//=============================================================================
int sci_readCOM(char *fname)
{
	SciErr sciErr;
	int m1 = 0, n1 = 0;
	int *piAddressVarOne = NULL;
	double dVarOne = 0;
	int iType1 = 0;

	int result = (int)FALSE;

	int bytesToRead = 0;
	int bytesReaded = 0;
	BOOL bReadString = FALSE;
	BOOL bOK = FALSE;

	char *BufferString = NULL;
	BYTE *BufferBytes = NULL;

	initializeStructPorts();

	CheckRhs(2,3);
	CheckLhs(1,3);

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if(getScalarDouble(pvApiCtx, piAddressVarOne, &dVarOne))
	{
		printError(&sciErr, 0);
		Scierror(999,"%s: Wrong size for input argument #%d: A scalar expected.\n", fname, 1);
		return 0;
	}

    if (!isUsedPortId((int)dVarOne))
    {
        result = (int)FALSE;
        bytesToRead = 0;
        bytesReaded = 0;
    }
    else
    {
		int m2 = 0, n2 = 0;
		int *piAddressVarTwo = NULL;
		double *pdVarTwo = NULL;
		int iType2 = 0;

		sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		sciErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		if ( iType2 != sci_matrix )
		{
			Scierror(999,"%s: Wrong type for input argument #%d: A scalar expected.\n", fname, 2);
			return 0;
		}

		sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarTwo, &m2, &n2, &pdVarTwo);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		if ( (m2 != n2) && (n2!= 1) )
		{
			Scierror(999,"%s: Wrong size for input argument #%d: A scalar expected.\n", fname, 2);
			return 0;
		}

		bytesToRead = (int)pdVarTwo[0];

		if (Rhs == 3)
		{
			int m3 = 0, n3 = 0;
			int *piAddressVarThree = NULL;
			int iType3 = 0;
			char *strVarThree = NULL;
			int lenVarThree = 0;

			sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
			if(sciErr.iErr)
			{
				printError(&sciErr, 0);
				return 0;
			}

			sciErr = getMatrixOfString(pvApiCtx, piAddressVarThree, &m3, &n3, &lenVarThree, &strVarThree);
			if(sciErr.iErr)
			{
				printError(&sciErr, 0);
				return 0;
			}

			if ( (m3 != n3) && (n3 != 1) )
			{
				Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n", fname, 3);
				return 0;
			}

			strVarThree = (char*)malloc(sizeof(char)*(lenVarThree + 1));
			if (strVarThree)
			{
				sciErr = getMatrixOfString(pvApiCtx, piAddressVarThree, &m3, &n3, &lenVarThree, &strVarThree);
				if(sciErr.iErr)
				{
					printError(&sciErr, 0);
					return 0;
				}

				if ( (strcmp(strVarThree, PARAM3_STRING) == 0) || (strcmp(strVarThree, PARAM3_BYTES) == 0) )
				{
					if (strcmp(strVarThree, PARAM3_STRING) == 0)
					{
						bReadString = TRUE;
					}
					else
					{
						bReadString = FALSE;
					}

					free(strVarThree);
					strVarThree = NULL;
				}
				else
				{
					free(strVarThree);
					strVarThree = NULL;
					Scierror(999,"%s: Wrong value for input argument #%d: 'string' or 'bytes' expected.\n", fname, 3);
					return 0;
				}
			}
			else
			{
				Scierror(999,"%s: error memory allocation.\n", fname);
				return 0;
			}
		}
		else
		{
			bReadString = FALSE;
		}
	}

	if (bReadString)
	{
		BufferString = (char*)malloc(sizeof(char) * (bytesToRead + 1));
        memset(BufferString, 0x00, bytesToRead + 1);
		if (BufferString == NULL)
		{
			Scierror(999,"%s: error memory allocation.\n", fname);
			return 0;
		}

		bOK = readString((int)dVarOne, BufferString, bytesToRead, &bytesReaded);
        sciprint("%d == %d[%s]\n", bytesToRead, bytesReaded, BufferString);
        //bOK = bytesToRead == bytesReaded;
        BufferString[bytesToRead] = '\0';
	}
	else
	{
		BufferBytes = (char*)malloc(sizeof(BYTE) * (bytesToRead + 1));
		if (BufferBytes == NULL)
		{
			Scierror(999,"%s: error memory allocation.\n", fname);
			return 0;
		}
		bOK = readBytes((int)dVarOne, BufferBytes, bytesToRead, &bytesReaded);
	}

	if (!bOK)
	{
		Scierror(999,"%s: error read on port %d.\n", fname, (int)dVarOne);
		return 0;
	}

	if (bReadString)
	{
		sciErr = createMatrixOfString(pvApiCtx, Rhs + 1, 1, 1, &BufferString);
		free(BufferString);
		BufferString = NULL;
	}
	else
	{
		double *pDouble = bytesToDoubles(BufferBytes, bytesReaded);
		sciErr = createMatrixOfDouble(pvApiCtx, Rhs + 1, 1, bytesReaded, pDouble);
		free(BufferBytes);
		BufferBytes = NULL;
		free(pDouble);
		pDouble = NULL;
	}

	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	LhsVar(1) = Rhs + 1;

	if (Lhs > 1)
	{
		if(createScalarDouble(pvApiCtx, Rhs + 2, bytesReaded))
		{
			printError(&sciErr, 0);
			return 0;
		}
		LhsVar(2) = Rhs + 2;
	}

	if (Lhs > 2)
	{
		if(createScalarDouble(pvApiCtx, Rhs + 3, bytesToRead))
		{
			printError(&sciErr, 0);
			return 0;
		}
		LhsVar(3) = Rhs + 3;
	}

    PutLhsVar();

	return 0;
}
//=============================================================================
double* bytesToDoubles(BYTE *pBytes, int iSize)
{
	double *result = NULL;
	if (pBytes)
	{
		if (iSize > 0)
		{
			int i = 0;
			result = (double*)malloc(sizeof(double) * iSize);
			for (i = 0; i < iSize; i++)
			{
				result[i] = (double)pBytes[i];
			}
		}
	}
	return result;
}
//=============================================================================
