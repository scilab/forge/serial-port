//=============================================================================
// Allan CORNET
// DIGITEO - 2010
// CeCill
//=============================================================================
#ifndef __PORTS_MANAGER_H__
#define __PORTS_MANAGER_H__

#ifdef _MSC_VER
	#include <windows.h>
#else
#endif

#include "serial.h"

#define PORTS_MAX 9
#define DEFAULT_BUFFER_IN_SIZE 4096
#define DEFAULT_BUFFER_OUT_SIZE 4096
#define DEFAULT_BAUDRATE 9600
#ifdef _MSC_VER
#define DEFAULT_PARITY NOPARITY
#else
#define DEFAULT_PARITY 0
#endif
#define DEFAULT_PROTOCOL 0
#ifdef _MSC_VER
#define DEFAULT_STOPBITS ONESTOPBIT
#else
#define DEFAULT_STOPBITS 0
#endif
#define DEFAULT_BYTESIZE 8
#define DEFAULT_TIMEOUT 50

typedef struct {
	HANDLE_PORT g_hCOM;
	BOOL bUsed;
	char portname[64];
	int baudrate;
	int parity;
	int stopBits;
	int byteSize;
	int timeout;
	int protocol;
} COM_port;

/**
*
*/
BOOL initializeStructPorts(void);

/**
*
*/
COM_port * getPortInfo(int iID);

/**
*
*/
BOOL isUsedPort(char* _pstPortName);

/**
*
*/
BOOL isUsedPortId(int iID);

/**
*
*/
HANDLE_PORT getHandleCOM(int iID);

/**
*
*/
BOOL writeString(int iID, char *bufferToWrite, int *BytesWritten);

/**
*
*/
BOOL writeBytes(int iID, BYTE *bufferToWrite, int lenBuffer, int *BytesWritten);

/**
*
*/
BOOL readString(int iID, char *bufferRead, int sizeToRead, int *bytesReaded);

/**
*
*/
BOOL readBytes(int iID, BYTE *bufferRead, int sizeToRead, int *bytesReaded);

/**
*
*/
BOOL openPortCOM(char* _pstPortName, int baudrate, int bytesize, int parity, int protocol,
							 int stopbits, int timeout);

/**
*
*/
BOOL closePortCOM(int nId);

/**
*
*/
BOOL purgePortCOM(int nId);

/**
*
*/
BOOL statusPortCOM(int nId, int* _piIn, int* _piOut);

#endif /* __PORTS_MANAGER_H__ */
//=============================================================================
