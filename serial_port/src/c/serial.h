//=============================================================================
// Allan CORNET
// DIGITEO - 2010
// CeCill
//=============================================================================
#ifndef __SERIAL_H__
#define __SERIAL_H__
//=============================================================================
#ifdef _MSC_VER
#include <windows.h>
#define HANDLE_PORT HANDLE
#else
#include <termios.h>
#define HANDLE_PORT int
#define BOOL int
#define BYTE char
#define TRUE 1
#define FALSE 0
#endif
//=============================================================================
/**
*
*/
HANDLE_PORT OpenCOM(char* _pstPortName, int baudrate, int parity, int protocol, int timeout, int stopbits,
							int bytesize, int *ierr);

/**
*
*/
BOOL CloseCOM(HANDLE_PORT g_hCOM);

/**
*
*/
BOOL ReadCOM(void* buffer, int nBytesToRead, int* pBytesRead, HANDLE_PORT g_hCOM);

/**
*
*/
BOOL WriteCOM(void* buffer, int nBytesToWrite, int* pBytesWritten,
							HANDLE_PORT g_hCOM);

/**
*
*/
BOOL PurgeCOM(HANDLE_PORT g_hCOM);

/**
*
*/
BOOL StatusCOM(int* _piIn, int* _piOut, HANDLE_PORT g_hCOM);

/**
*
*/
BOOL checkPortValue(int port);

/**
*
*/
BOOL checkBaudRate(int baudrate);

/**
*
*/
BOOL checkParity(int parity);

/**
*
*/
BOOL checkByteSize(int bytesize);

/**
*
*/
BOOL checkStopBits(int stopbits);

/**
*
*/
BOOL checkTimeOut(int timeout);

#endif /* __SERIAL_H__ */
//=============================================================================
