//=============================================================================
// Allan CORNET
// DIGITEO - 2010
// CeCill
//=============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "serial.h"
/* http://www.easysw.com/~mike/serial/serial.html */
/* http://www.opengroup.org/onlinepubs/009695399/basedefs/termios.h.html */
//=============================================================================
HANDLE_PORT OpenCOM(char* _pstPortName, int baudrate, int parity, int protocol,int timeout,
									 	int stopbits, int bytesize, int *ierr)
{
	struct termios options;

	HANDLE_PORT g_hCOM = 0;

	g_hCOM = open(_pstPortName, O_RDWR|O_NOCTTY| O_NDELAY);

	cfsetispeed(&options, baudrate);
	cfsetospeed(&options, baudrate);

	switch(parity)
	{
		case 0: /* NOPARITY */
		{
			options.c_cflag &= ~PARENB;
			options.c_cflag &= ~CSTOPB;
			options.c_cflag &= ~CSIZE;
			options.c_cflag |= CS8;
		}
		break;
		case 1: /* ODDPARITY */
		{
			options.c_cflag |= PARENB;
			options.c_cflag |= PARODD;
			options.c_cflag &= ~CSTOPB;
			options.c_cflag &= ~CSIZE;
			options.c_cflag |= CS7;
		}
		break;
		case 2: /* EVENPARITY */
		{
			options.c_cflag |= PARENB;
			options.c_cflag &= ~PARODD;
			options.c_cflag &= ~CSTOPB;
			options.c_cflag &= ~CSIZE;
			options.c_cflag |= CS7;
		}
		break;
		case 3: /* MARKPARITY */
		{
			options.c_cflag &= ~PARENB;
			options.c_cflag |= CSTOPB;
			options.c_cflag &= ~CSIZE;
			options.c_cflag |= CS7;
		}
		break;
		case 4: /* SPACEPARITY */
		{
			options.c_cflag &= ~PARENB;
			options.c_cflag &= ~CSTOPB;
			options.c_cflag &= ~CSIZE;
			options.c_cflag |= CS8;
		}
		break;
	}


	tcgetattr(g_hCOM, &options);
	*ierr = 0;

	PurgeCOM(g_hCOM);

    sleep(2);
	return g_hCOM;
}
//=============================================================================
BOOL CloseCOM(HANDLE_PORT g_hCOM)
{
	tcflush(g_hCOM, TCIOFLUSH);
	close(g_hCOM);
}
//=============================================================================
BOOL ReadCOM(void* buffer, int nBytesToRead, int* pBytesRead, HANDLE_PORT g_hCOM)
{
	*pBytesRead = read(g_hCOM, buffer, nBytesToRead);
}
//=============================================================================
BOOL WriteCOM(void* buffer, int nBytesToWrite, int* pBytesWritten,
							HANDLE_PORT g_hCOM)
{
	*pBytesWritten = write(g_hCOM, buffer, nBytesToWrite);
	if (tcdrain(g_hCOM) != 0)
	{
		return FALSE;
	}
	return TRUE;
}
//=============================================================================
BOOL PurgeCOM(HANDLE_PORT g_hCOM)
{
	if ((tcflush(g_hCOM, TCIFLUSH) != 0) || (tcflush(g_hCOM, TCOFLUSH) != 0))
	{
		return FALSE;
	}
	return TRUE;
}
//=============================================================================
BOOL StatusCOM(int* _piIn, int* _piOut, HANDLE_PORT g_hCOM)
{
    int res = 0;
    *_piOut = 0;
    res = ioctl(g_hCOM, FIONREAD, _piIn);
    return res;
}
//=============================================================================
BOOL checkPortValue(int port)
{
	if ((port > 0) && (port < 10)) return TRUE;
	return FALSE;
}
//=============================================================================
BOOL checkBaudRate(int baudrate)
{
	switch(baudrate)
	{
		case 110: case 300:
		case 600: case 1200:
		case 2400: case	4800:
		case 9600: case 14400:
		case 19200: case 38400:
		case 57600: case 115200:
		case 128000: case	256000:
			return TRUE;
		break;
		default:
		break;
	}
	return FALSE;
}
//=============================================================================
BOOL checkParity(int parity)
{
    switch (parity)
    {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
        return TRUE;
    default:
        return FALSE;
    }
}
//=============================================================================
BOOL checkByteSize(int bytesize)
{
	switch(bytesize)
	{
		case 5: case 6: case 7: case 8:
			return TRUE;
		break;
	}

	return FALSE;
}
//=============================================================================
BOOL checkStopBits(int stopbits)
{
	switch (stopbits)
	{
		case 0: case 1: case 2:
			return TRUE;
		break;
	}
	return FALSE;
}
//=============================================================================
BOOL checkTimeOut(int timeout)
{
	if (timeout >= 0) return TRUE;
	return FALSE;
}
//=============================================================================
BOOL checkProtocol(int protocol)
{
	switch(protocol)
	{
		case 0: case 1: case 2:
			return TRUE;
		break;
	}
	return FALSE;
}
//=============================================================================
