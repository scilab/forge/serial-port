//=============================================================================
// Allan CORNET
// DIGITEO - 2010
// CeCill
//=============================================================================
#include <stdio.h>
#include <stdlib.h>
#include "serial.h"
//=============================================================================
HANDLE OpenCOM(char* _pstPortName, int baudrate, int parity, int protocol, int timeout, int stopbits, int bytesize, int *ierr)
{
    HANDLE hCOM = NULL;
    
    COMMTIMEOUTS cto;
    DCB dcb;
    
    hCOM = CreateFile(_pstPortName, GENERIC_READ|GENERIC_WRITE, 0, NULL,
                        OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
                        
    if(hCOM == INVALID_HANDLE_VALUE)
    {
        *ierr = 1;
        return NULL;
    }

	dcb.DCBlength=sizeof(dcb);
	if (!GetCommState(hCOM, &dcb))
    {
        *ierr = 1;
		 return NULL;
	}

	dcb.BaudRate = baudrate;
	dcb.ByteSize = bytesize;
	dcb.StopBits = stopbits;
	dcb.Parity = parity;

	if(!SetCommState(hCOM, &dcb))
    {
        *ierr = 1;
		 return NULL;
	}
    
	cto.ReadIntervalTimeout=timeout;
	cto.ReadTotalTimeoutConstant=timeout;
	cto.ReadTotalTimeoutMultiplier=1;
	cto.WriteTotalTimeoutConstant=timeout;
	cto.WriteTotalTimeoutMultiplier=1;
	if(!SetCommTimeouts(hCOM, &cto))
    {
		*ierr = 1;
	    return NULL;
	}

    //let's time to process to really open file
    Sleep(2000);
    *ierr = 0;
    return hCOM;
}
//=============================================================================
BOOL PurgeCOM(HANDLE g_hCOM)
{
	 return PurgeComm(g_hCOM, PURGE_TXCLEAR|PURGE_RXCLEAR|PURGE_TXABORT|PURGE_RXABORT);
}
//=============================================================================
BOOL CloseCOM(HANDLE g_hCOM)
{
    /* Close port*/
    return CloseHandle(g_hCOM);
}
//=============================================================================
BOOL ReadCOM(void* buffer, int nBytesToRead, int* pBytesRead, HANDLE g_hCOM)
{
    return ReadFile(g_hCOM, buffer, nBytesToRead, pBytesRead, NULL);
}
//=============================================================================
BOOL WriteCOM(void* buffer, int nBytesToWrite, int* pBytesWritten, HANDLE g_hCOM)
{
    BOOL ret1 = WriteFile(g_hCOM, buffer, nBytesToWrite, pBytesWritten, NULL);
    //BOOL ret2 = FlushFileBuffers(g_hCOM);
    return ret1;/* && ret2*/;
}
//=============================================================================
BOOL StatusCOM(int* _piIn, int* _piOut, HANDLE g_hCOM)
{
    DWORD dwErrorFlags = 0;
    COMSTAT ComStat;

    BOOL res = ClearCommError(g_hCOM, &dwErrorFlags, &ComStat);
    if(res == 0 || dwErrorFlags)
    {
        return FALSE;
    }

    *_piIn = ComStat.cbInQue;
    *_piOut = ComStat.cbOutQue;
    return TRUE;
}
//=============================================================================
BOOL checkPortValue(int port)
{
	if ((port > 0) && (port < 10)) return TRUE;
	return FALSE;
}
//=============================================================================
BOOL checkBaudRate(int baudrate)
{
	switch(baudrate)
	{
		case CBR_110: case CBR_300:
		case CBR_600: case CBR_1200:
		case CBR_2400: case	CBR_4800:
		case CBR_9600: case CBR_14400:
		case CBR_19200: case CBR_38400:
		case CBR_57600: case CBR_115200:
		case CBR_128000: case	CBR_256000:
			return TRUE;
		break;
		default:
		break;
	}
	return FALSE;
}
//=============================================================================
BOOL checkByteSize(int bytesize)
{
	switch(bytesize)
	{
		case 5: case 6: case 7: case 8:
			return TRUE;
		break;
	}
	
	return FALSE;
}
//=============================================================================
BOOL checkStopBits(int stopbits)
{
	switch (stopbits)
	{
		case ONESTOPBIT: case ONE5STOPBITS: case TWOSTOPBITS:
			return TRUE;
		break;
	}
	return FALSE;
}
//=============================================================================
BOOL checkTimeOut(int timeout)
{
	if (timeout >= 0) return TRUE;
	return FALSE;
}
//=============================================================================
BOOL checkParity(int parity)
{
	switch(parity)
	{
		case NOPARITY: case ODDPARITY:
		case EVENPARITY: case MARKPARITY: case SPACEPARITY:
			return TRUE;
		break;
	}
	return FALSE;
}
//=============================================================================
BOOL checkProtocol(int protocol)
{
	switch(protocol)
	{
		case 0: case 1: case 2:
			return TRUE;			
		break;
	}
	return FALSE;	
}
//=============================================================================
