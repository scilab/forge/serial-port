// This file is released under the 3-clause BSD license. See COPYING-BSD.

// This macro compiles the files

function builder_c()
    src_c_path = get_absolute_file_path('builder_c.sce');

    CFLAGS = ilib_include_flag(src_c_path);

    c_files = ['ports_manager.c'];

    if getos() == "Windows" then
        c_files = [c_files, 'serial_windows.c'];
    else
        c_files = [c_files, 'serial_others.c'];
    end

    tbx_build_src(['serial_port'], c_files, 'c', ..
                  src_c_path, '', '', CFLAGS);
endfunction

builder_c();
clear builder_c; // remove builder_c on stack
