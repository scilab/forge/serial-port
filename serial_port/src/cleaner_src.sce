// ====================================================================
// Allan CORNET
// DIGITEO - 2010
// ====================================================================
src_dir = get_absolute_file_path("cleaner_src.sce");
// ====================================================================
for language = ["c"]
	cleaner_file = src_dir + filesep() + language + filesep() + "cleaner.sce";
	if fileinfo(cleaner_file) <> [] then
		exec(cleaner_file);
		mdelete(cleaner_file);
	end
end
// ====================================================================
clear src_dir;
// ====================================================================
